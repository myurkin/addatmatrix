import argparse
import numpy as np
from addatmatrix.t_matrix import t_matrix_arbitrary_geom_pol

shape = "cylinder"
n = 2.5

radius = 250 
h = 300

wl = 914 # treams element 55

d = 2 * radius

ar = h/d
x = 2*np.pi*radius/wl
lmax = 2

T = t_matrix_arbitrary_geom_pol(shape, [ar], rad=x,ar=ar,n=n,lmax=lmax)

np.save("Tcyl",T)


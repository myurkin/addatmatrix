import argparse
import numpy as np
from addatmatrix.t_matrix import t_matrix_arbitrary_geom_pol_aniso

shape = "box"

nx = 1.9
ny = 2.
nz = 2.0976177

m=f"{nx} 0.0 {ny} 0.0 {nz} 0.0"

size_x = 250.0
size_y = 200.0
size_z = 150.0

wl = 600 # treams element 55

x = 2* np.pi * size_x/2 /wl
lmax = 2

ar = size_z / size_x

T = t_matrix_arbitrary_geom_pol_aniso(shape, [size_y/size_x, size_z/size_x], m, rad=x, ar=ar, lmax=lmax)

np.save("Tbox",T)

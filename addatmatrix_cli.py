#!/usr/bin/env python3
import addatmatrix.t_matrix
import addatmatrix.cross_section
import addatmatrix.data
import addatmatrix.wrapper
import click

@click.group()
def cli():
    """A simple CLI."""
    pass

@click.command()
@click.option("--adda_string", default="-shape sphere")
@click.option("--max_order", default=3, help="Maximal multipole order (l_max in Smuthi convention).")
@click.option('--num_theta', default=91, help='Number of polar angles for the scattered field integration grid.')
@click.option('--num_phi', default=181, help='Number of azimuthal angles for the scattered field integration grid.')
@click.option('--num_illuminations', default=20, help='Number of plane-wave illuminations per polarization.')
@click.option('--particle_z',default=0., help='Particle center position z-coordinate relative to the origin center')
@click.option('--output_path',default="tmatrix.h5", help='Path of the output T-matrix h5 file')
@click.option('--wavelength',default=0., help="Wavelength for metadata purposes")
def tmatrix(adda_string, max_order, num_theta, num_phi, num_illuminations, particle_z, output_path, wavelength):
    if particle_z == 0:
        prognosis = addatmatrix.wrapper.prognose_from_adda_string(adda_string)
        particle_z = addatmatrix.wrapper.get_particle_z(prognosis)
    t = addatmatrix.t_matrix.t_matrix_from_adda_string(num_theta, num_phi, num_illuminations, 
                                                       max_order, adda_string,
                                                       particle_z)
    tmatrix_data, indices = addatmatrix.data.prepare_data_for_storage(t, max_order)
    metadata = addatmatrix.data.get_metadata(adda_string)
    if "wavelength" not in metadata.keys():
        metadata["wavelength"] = wavelength
    metadata["num_illuminations"] = num_illuminations
    metadata["num_theta"] = num_theta
    metadata["num_phi"] = num_phi
    addatmatrix.data.write_t_matrix_data(output_path, tmatrix_data, indices, metadata)

@click.command()
@click.argument('min_wavelength',type=click.FLOAT)
@click.argument('max_wavelength',type=click.FLOAT)
@click.argument('num_wavelength',type=click.INT)
@click.option('--adda_string')
@click.option('--output_path',default=None)
def spectrum(min_wavelength, max_wavelength, num_wavelength, adda_string, output_path):
    csx, csy = addatmatrix.cross_section.get_cross_section(min_wavelength, max_wavelength, num_wavelength, adda_string)
    addatmatrix.data.write_cross_section(csx, csy, output_path)

cli.add_command(tmatrix)
cli.add_command(spectrum)

if __name__ == '__main__':
    cli()

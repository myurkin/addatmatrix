import h5py
import numpy as np


def get_smuthi_indices(lmax):
    l = [l for l in range(1,lmax+1) for m in range(-l,l+1)]
    m = [m for l in range(1,lmax+1) for m in range(-l,l+1)]
    l = np.array(2*l)
    m = np.array(2*m)
    pol = np.array([0]*(len(l)//2) + [1]*(len(l)//2))
    ind = np.array(range(len(l)))
    return l, m, pol, ind

def rearrange_blocks(T, lmax):
    l = [l for l in range(1,lmax+1) for m in range(-l,l+1)]
    num_block = len(l)
    T_mm = T[:num_block,:num_block]
    T_ee = T[num_block:,num_block:]
    T_em = T[num_block:,:num_block]
    T_me = T[:num_block,num_block:]

    T_arr = np.block([[T_ee,T_em],[T_me,T_mm]])
    return T_arr

def convert_indices(pol, m, l, ind):
    pol_conv = pol.reshape(2,-1).T.flatten()
    m_conv = m.reshape(2,-1).T.flatten()
    l_conv = l.reshape(2,-1).T.flatten()
    ind_conv = ind.reshape(2,-1).T.flatten()
    return pol_conv, m_conv, l_conv, ind_conv

def normalize_t_matrix(t_matrix, m_conv):
    m_a,m_b = np.meshgrid(m_conv,m_conv)
    norm_factor_t = (-1.0)**(m_a-m_b)
    return norm_factor_t * t_matrix

def convert_t_matrix(T, ind_conv):
    T_conv = T[ind_conv][:,ind_conv]
    return T_conv

def prepare_data_for_storage(T, lmax):
    l, m, pol, ind = get_smuthi_indices(lmax)
    pol_conv, m_conv, l_conv, ind_conv = convert_indices(pol, m, l, ind)
    T_arr = rearrange_blocks(T, lmax)
    T_conv = convert_t_matrix(T_arr, ind_conv)
    T_norm = normalize_t_matrix(T_conv, m_conv)
    indices = (l_conv, m_conv, pol_conv)
    return T_norm, indices

def get_metadata(adda_string=None):
    if adda_string is None:
        with open("log","r") as f:
            data = f.readlines()
        adda_command_split = data[1].split("-")[1:]
    else:
        adda_command_split = adda_string.split("-")
    metadata = {ind.split()[0]:ind.split()[1:] for ind in adda_command_split[1:]}
    return metadata

def write_t_matrix_data(path, tmatrix_data, indices, metadata):
    l, m, pol = indices
    mstring = metadata["m"]
    m = float(mstring[0]) + 1j * float(mstring[1])
    relative_permittivity = m ** 2
    relative_permeability = 1.0

    # Create the HDF5 file
    with h5py.File(path, 'w') as h5file:
        # Creating groups
        computation_group = h5file.create_group('computation')
        embedding_group = h5file.create_group('embedding')
        modes_group = h5file.create_group('modes')
        scatterer_group = h5file.create_group('scatterer')

        # Creating datasets
        h5file.create_dataset('vacuum_wavelength', data=metadata["wavelength"])
        
        mesh_msh_data = "Mesh data as a string or binary blob"  # Example data for mesh.msh
        h5file.create_dataset('mesh.msh', data=np.void(mesh_msh_data.encode('utf-8')))
        
        h5file.create_dataset('tmatrix', data=tmatrix_data)
        
        # Adding sub-groups and datasets in computation group
        method_parameters = computation_group.create_group('method_parameters')
        for key, val in metadata.items():
            method_parameters.create_dataset(key, data=np.string_(val))
        
        # Adding sub-groups and datasets in embedding group
        embedding_group.create_dataset('relative_permeability', data=1.0)
        embedding_group.create_dataset('relative_permittivity', data=1.0)
        
        # Adding sub-groups and datasets in modes group
        modes_group.create_dataset('l', data=l)
        modes_group.create_dataset('m', data=m)
        modes_group.create_dataset('polarization', data=pol)
        
        # Adding sub-groups and datasets in scatterer group
        geometry_group = scatterer_group.create_group('geometry')
        material_group = scatterer_group.create_group('material')
        material_group.create_dataset('relative_permeability', data=1.0)
        material_group.create_dataset('relative_permittivity', data=1.0)

def write_cross_section(csx,csy,output_path):
    np.savez(output_path, csx, csy)
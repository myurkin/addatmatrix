import addatmatrix.wrapper
import sys
import addatmatrix.far_field
import subprocess
import numpy as np
import tqdm

def get_cross_section(min_wavelength, max_wavelength, num_wavelength, adda_string):
    wavelength_range = np.linspace(min_wavelength, max_wavelength, num_wavelength)

    print(sys.argv[4])

    csx = []
    csy = []
    for wl in tqdm.tqdm(wavelength_range):
        adda_output = addatmatrix.wrapper.run_from_args(adda_string+f" -lambda {wl}")
        csx.append(adda_output.cross_section_x)
        csy.append(adda_output.cross_section_y)

    csx = np.array(csx)
    csy = np.array(csy)
    return csx, csy

if __name__ == "__main__":
    min_wavelength = float(sys.argv[1])
    max_wavelength = float(sys.argv[2])
    num_wavelength = int(sys.argv[3])
    adda_string = sys.argv[4]

    csx, csy = get_cross_section(min_wavelength, max_wavelength, num_wavelength, adda_string)

    np.save("csx",csx)
    np.save("csy",csy)


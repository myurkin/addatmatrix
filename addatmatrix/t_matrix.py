import numpy as np
from tqdm import trange
from scipy.linalg import pinv

from addatmatrix import far_field
from addatmatrix.wrapper import ellipsoid_pol, geom_from_file, arbitrary_geom_pol, arbitrary_geom_pol_aniso, pol_from_adda_string
from addatmatrix.math import double_trapezoid_integral, pwe_to_swe_conversion, spherical_harmonic_m, spherical_harmonic_n
from addatmatrix.spatial import IntegrationGrid, FibonacciIlluminationGrid, UniformIntegrationGrid

_EPS = 1e-2
ILLUMINATION_GRID = FibonacciIlluminationGrid

def solve_t_matrix(scat_tot: np.array, inc_tot: np.array) -> np.array:
    scat_tot = np.array(scat_tot).T
    inc_tot = np.array(inc_tot).T
    inc_tot_inv = pinv(inc_tot)
    T = scat_tot @ inc_tot_inv
    return T

def calculate_scattered_field_coefficients(Escat: np.array, ks: float,
                                           lmax: int, grid: IntegrationGrid) -> np.array:
    scatf1 = np.zeros(2 * lmax * (lmax + 2), dtype=complex)
    ii = 0
    for tau in [0, 1]:
        for l in range(1, lmax + 1):
            for m in range(-l, l + 1):
                if tau:
                    mr = spherical_harmonic_m(grid.T, grid.P, l, m)
                else:
                    mr = 1j * spherical_harmonic_n(grid.T, grid.P, l, m)
                prefactor = ks / (-1j) ** (l + 1)
                int1 = double_trapezoid_integral(mr.conj() * Escat * prefactor, grid)
                int2 = double_trapezoid_integral(mr.conj() * mr, grid) # TODO: use normalization
                scatf1[ii] = int1 / int2
                ii += 1
    return scatf1

def t_matrix_spheroid(rad=1.,ar=1.5,n=2,lmax=3,nsub=1.00001+0j,
                      Nil=20,Nphi=181,Ntheta=91,Ngrid=20,ddaeps=2.5,
                      ks = 1, seps = 1e-2):
    z = rad * ar

    grid = UniformIntegrationGrid(Nphi, Ntheta)
    illumination_grid = ILLUMINATION_GRID(illumination_number=Nil)
    angle_grid = illumination_grid.get_angle_grid()

    scat_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    inc_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    
    for iang in trange(2 * Nil):
        # TODO: PWE objects
        pol, kth, kph = angle_grid[iang]


        ellipsoid_pol(rad, n, yr=1, zr=ar, grid=Ngrid,
                      eps=ddaeps, kth=kth, kph=kph, ms=nsub)

        data = far_field.read_pol_file(pol)
        pol_cart, r = far_field.parse_pol_file(data)
        pol_eff = far_field.get_total_polarization(pol_cart, grid.er, r, ks)
        pol_eff_sph = far_field.convert_cartesian_to_spherical(pol_eff, grid)
        Escat = far_field.calculate_far_field(pol_eff_sph, ks)
        pos = [0, 0, z]
        kth_arr = np.array([kth])
        azimuthal_angle = np.array([kph])[:,None]
        incf = pwe_to_swe_conversion(lmax, lmax, pos, pos, int(pol), np.sin(kth_arr), 
                                     np.cos(kth_arr), azimuthal_angle)
        
        scatf1=calculate_scattered_field_coefficients(Escat, ks, lmax, grid)
        scat_tot[iang, :] = scatf1
        inc_tot[iang, :] = incf

    T = solve_t_matrix(scat_tot, inc_tot)
    return T

def t_matrix_arbitrary_geom(rad=1.,ar=1.5,n=2,lmax=3,nsub=1.00001+0j,
                      Nil=20,Nphi=181,Ntheta=91,Ngrid=20,ddaeps=2.5,
                      ks = 1, seps = 1e-2):
    z = rad * ar

    grid = UniformIntegrationGrid(Nphi, Ntheta)
    illumination_grid = ILLUMINATION_GRID(illumination_number=Nil)
    angle_grid = illumination_grid.get_angle_grid()

    scat_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    inc_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    
    for iang in trange(2 * Nil):
        # TODO: PWE objects
        pol, kth, kph = angle_grid[iang]

        adda_output = geom_from_file(rad, n, yr=1, zr=ar, grid=Ngrid,
                      eps=ddaeps, kth=kth, kph=kph, ms=nsub)

        data = adda_output.dipole_polarization_x if pol == 1 else adda_output.dipole_polarization_y
        pol_cart, r = far_field.parse_pol_file(data)
        pol_eff = far_field.get_total_polarization(pol_cart, grid.er, r, ks)
        pol_eff_sph = far_field.convert_cartesian_to_spherical(pol_eff, grid)
        Escat = far_field.calculate_far_field(pol_eff_sph, ks)
        pos = [0, 0, z]
        kth_arr = np.array([kth])
        azimuthal_angle = np.array([kph])[:,None]
        incf = pwe_to_swe_conversion(lmax, lmax, pos, pos, int(pol), np.sin(kth_arr), 
                                     np.cos(kth_arr), azimuthal_angle)
        
        scatf1 = calculate_scattered_field_coefficients(Escat, ks, lmax, grid)
        scat_tot[iang, :] = scatf1
        inc_tot[iang, :] = incf

    T = solve_t_matrix(scat_tot, inc_tot)
    return T

def t_matrix_arbitrary_geom_pol(
        shape, params, rad=1.,ar=1.5,n=2,lmax=3,nsub=1.00001+0j,
        Nil=20,Nphi=181,Ntheta=91,Ngrid=20,ddaeps=2.5,
        ks = 1, seps = 1e-2):
    z = rad * ar

    grid = UniformIntegrationGrid(Nphi, Ntheta)
    illumination_grid = ILLUMINATION_GRID(illumination_number=Nil)
    angle_grid = illumination_grid.get_angle_grid()

    scat_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    inc_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    
    for iang in trange(2 * Nil):
        # TODO: PWE objects
        pol, kth, kph = angle_grid[iang]

        adda_output = arbitrary_geom_pol(shape, params, n, rad, ar, 
                                         Ngrid, ddaeps, Ntheta, Nphi,
                                         kth, kph, nsub)

        data = adda_output.dipole_polarization_x if pol == 1 else adda_output.dipole_polarization_y
        pol_cart, r = far_field.parse_pol_file(data)
        pol_eff = far_field.get_total_polarization(pol_cart, grid.er, r, ks)
        pol_eff_sph = far_field.convert_cartesian_to_spherical(pol_eff, grid)
        Escat = far_field.calculate_far_field(pol_eff_sph, ks)
        pos = [0, 0, z]
        kth_arr = np.array([kth])
        azimuthal_angle = np.array([kph])[:,None]
        incf = pwe_to_swe_conversion(lmax, lmax, pos, pos, int(pol), np.sin(kth_arr), 
                                     np.cos(kth_arr), azimuthal_angle)
        
        scatf1 = calculate_scattered_field_coefficients(Escat, ks, lmax, grid)
        scat_tot[iang, :] = scatf1
        inc_tot[iang, :] = incf

    T = solve_t_matrix(scat_tot, inc_tot)
    return T

def t_matrix_arbitrary_geom_pol_aniso(
        shape, params, n, rad=1.,ar=1.5,lmax=3,nsub=1.00001+0j,
        Nil=20,Nphi=181,Ntheta=91,Ngrid=20,ddaeps=2.5,
        ks = 1, seps = 1e-2):
    z = rad * ar

    grid = UniformIntegrationGrid(Nphi, Ntheta)
    illumination_grid = ILLUMINATION_GRID(illumination_number=Nil)
    angle_grid = illumination_grid.get_angle_grid()

    scat_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    inc_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    
    for iang in trange(2 * Nil):
        # TODO: PWE objects
        pol, kth, kph = angle_grid[iang]

        arbitrary_geom_pol_aniso(shape, params, n, rad, ar, Ngrid, ddaeps, Ntheta, Nphi,
                           kth, kph, nsub)

        data = adda_output.dipole_polarization_x if pol == 1 else adda_output.dipole_polarization_y
        pol_cart, r = far_field.parse_pol_file(data)
        pol_eff = far_field.get_total_polarization(pol_cart, grid.er, r, ks)
        pol_eff_sph = far_field.convert_cartesian_to_spherical(pol_eff, grid)
        Escat = far_field.calculate_far_field(pol_eff_sph, ks)
        pos = [0, 0, z]
        kth_arr = np.array([kth])
        azimuthal_angle = np.array([kph])[:,None]
        incf = pwe_to_swe_conversion(lmax, lmax, pos, pos, int(pol), np.sin(kth_arr), 
                                     np.cos(kth_arr), azimuthal_angle)
        
        scatf1 = calculate_scattered_field_coefficients(Escat, ks, lmax, grid)
        scat_tot[iang, :] = scatf1
        inc_tot[iang, :] = incf

    T = solve_t_matrix(scat_tot, inc_tot)
    return T

def t_matrix_factory(Nphi, Ntheta, Nil, ks, lmax, adda_callable, **kwargs):
    if "z" not in kwargs.keys():
        z = kwargs["rad"] * kwargs["ar"]
    else:
        z = kwargs["z"]
    grid = UniformIntegrationGrid(Nphi, Ntheta)
    illumination_grid = ILLUMINATION_GRID(illumination_number=Nil)
    angle_grid = illumination_grid.get_angle_grid()

    scat_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    inc_tot = np.zeros((2 * Nil, 2 * lmax * (lmax + 2)), dtype=complex)
    
    for iang in trange(2 * Nil):
        # TODO: PWE objects
        pol, kth, kph = angle_grid[iang]

        adda_output = adda_callable(**kwargs, kth=kth, kph=kph, Nphi=Nphi, Ntheta=Ntheta)

        data = adda_output.dipole_polarization_x if pol == 1 else adda_output.dipole_polarization_y
        pol_cart, r = far_field.parse_pol_file(data)
        pol_eff = far_field.get_total_polarization(pol_cart, grid.er, r, ks)
        pol_eff_sph = far_field.convert_cartesian_to_spherical(pol_eff, grid)
        Escat = far_field.calculate_far_field(pol_eff_sph, ks)
        pos = [0, 0, z]
        kth_arr = np.array([kth])
        azimuthal_angle = np.array([kph])[:,None]
        incf = pwe_to_swe_conversion(lmax, lmax, pos, pos, int(pol), np.sin(kth_arr), 
                                     np.cos(kth_arr), azimuthal_angle)
        
        scatf1 = calculate_scattered_field_coefficients(Escat, ks, lmax, grid)
        scat_tot[iang, :] = scatf1
        inc_tot[iang, :] = incf

    T = solve_t_matrix(scat_tot, inc_tot)
    return T

def t_matrix_from_adda_string(Nphi, Ntheta, Nil, lmax, adda_string, z=0):
    ks = 1
    T = t_matrix_factory(Nphi, Ntheta, Nil, ks, lmax, pol_from_adda_string, adda_string=adda_string, z=z)
    return T

# addatmatrix

**addatmatrix** is a Python package that provides a convenient interface for calculating the T-matrix using the ADDA code.

## Installation

Before using **addatmatrix**, you need to compile ADDA. Please follow the instructions provided with ADDA for compilation.

Once ADDA is compiled, modify the `addawrapper.py` file in this package to set the correct path to the ADDA executable.

Install **addatmatrix** using pip:

```bash
pip install addatmatrix
```

